/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 21:16:28 by aeddi             #+#    #+#             */
/*   Updated: 2014/03/08 09:29:13 by pde-lacr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <puissance4.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ncurses.h>
#include <stdio.h>

int			init_ui(t_grid *grid)
{
	int		x;
	int		y;
	char	disp_char;
	WINDOW	*pad_ptr;
	t_ui	ui;

	initscr();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	ui.pad_line = grid->line * 2 + 1;
	ui.pad_col = grid->column * 3 + 1;
	pad_ptr = newpad(ui.pad_line, ui.pad_col);
	x = 0; // 1
	while (x < ui.pad_line)
	{
		y = 0; // 1
		while (y < ui.pad_col)
		{
			if (x % 2 == 0 && y % 3 == 0) // 1 1
				disp_char = '+';
			else if (x % 2 == 1 && y % 3 == 0) // 0 1
				disp_char = '|';
			else if (x % 2 == 0 && (y % 3 == 1 || y % 3 == 2)) // 1 0
				disp_char = '-';
			else
				disp_char = ' ';
			mvwaddch(pad_ptr, x, y, disp_char);
			y++;
		}
		x++;
	}
	ui.pad_top = 5;
	ui.pad_left = 2;
	ui.pad_maxline = LINES - 2;
	ui.pad_maxcol = COLS - 2;
	//if (COLS >= (int)grid->column + 2 && LINES >= (int)grid->line)
		prefresh(pad_ptr, 0, 0, 5, 2, ui.pad_maxline, ui.pad_maxcol);
		move(0, 10);
		//refresh();
	return (0);
}

t_ret			print_grid_ui(t_grid *grid, t_player cur_pl)
{
	static int	init = 1;

	if (init == 1)
		init = init_ui(grid);
	grid = NULL;
	cur_pl = 0;
	return (SUCCESS);
}

t_ret			print_grid_txt(t_grid *grid, t_player cur_pl)
{
	grid = NULL;
	cur_pl = 0;
	return (SUCCESS);
}

t_ret		ia_turn(t_grid *grid)
{
	grid = NULL;
	return (SUCCESS);
}

t_ret		player_turn(t_grid *grid)
{
	grid = NULL;
	return (SUCCESS);
}

t_ret		init_game(t_grid *grid, t_player cur_pl)
{
	t_ret	ret;

	ret = 0;
	while (42)
	{
		if ((grid->ui && print_grid_ui(grid, cur_pl) == FAIL)
				|| (!grid->ui && print_grid_txt(grid, cur_pl) == FAIL))
			return (FAIL);
		ret = (cur_pl == IA) ? ia_turn(grid) : player_turn(grid);
		if (ret == FAIL || ret == DONE)
			return (ret);
		cur_pl += (cur_pl + 1) % 2;
	}
}

t_ret		init_grid(int ac, char **av, t_grid *grid)
{
	size_t	grid_size;

	if (ac < 3 || ac > 4)
	{
		ft_putendl("Usage : ./puissance4 line_nb(6+) col_nb(7+) [-ui]");
		return (FAIL);
	}
	grid->line = ft_atoi(av[1]);
	grid->column = ft_atoi(av[2]);
	grid->ui = (ac == 4 && !ft_strcmp(av[3], "-ui")) ? 1 : 0;
	if (grid->line < 6 || grid->column < 7)
	{
		ft_putendl("Usage : ./puissance4 line_nb(6+) col_nb(7+) [-ui]");
		return (1);
	}
	grid_size = (grid->line * grid->column) / 8;
	if ((grid->player = (char *)ft_memalloc(sizeof(char *) * grid_size)) == NULL
		|| (grid->ia = (char *)ft_memalloc(sizeof(char *) * grid_size)) == NULL)
	{
		ft_putendl(strerror(errno));
		return (1);
	}
	return (0);
}

t_player	choose_first_player(void)
{
	srand(time(NULL));
	if (rand() % 2)
		return (PLAYER);
	return (IA);
}

int			main(int ac, char **av)
{
	t_grid		grid;
	t_player	first_pl;
	t_ret		ret;

	if (init_grid(ac, av, &grid))
		return (1);
	first_pl = choose_first_player();
	ret = init_game(&grid, first_pl);
	free(grid.player);
	free(grid.ia);
	if (ret == FAIL)
	{
		ft_putendl(strerror(errno));
		return (1);
	}
	return (0);
}
