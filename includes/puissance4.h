/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puissance4.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 21:26:13 by aeddi             #+#    #+#             */
/*   Updated: 2014/03/08 09:03:25 by pde-lacr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUISSANCE4_H
# define PUISSANCE4_H

typedef enum	e_ret
{
	SUCCESS = 0,
	DONE = 1,
	FAIL = 2
}				t_ret;

typedef enum	e_player
{
	PLAYER = 0,
	IA = 1
}				t_player;

typedef struct	s_grid
{
	size_t		line;
	size_t		column;
	int			ui;
	char		*player;
	char		*ia;
}				t_grid;

typedef struct	s_ui
{
	int			pad_line;
	int			pad_col;
	int			pad_maxline;
	int			pad_maxcol;
	int			pad_top;
	int			pad_left;
}				t_ui;

#endif /* !PUISSANCE4_H */
